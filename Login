package gui;

import javax.swing.JFrame;

public class Login extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String ID;
		
	public Login(String IDIn)
	{
		this.ID = IDIn;
	}
	
	@Override
	public String toString()
	{
		String returnString = null;
		
		if(this.ID == "1")
		{
			return ID + " - Martin Curran";
		}
		else if(this.ID == "2")
		{
			return ID + " - Shane Kilpatrick";
		}
		else if(this.ID == "3")
		{
			return ID + " - Patrick Magee";
		}
		else if(this.ID == "4")
		{
			return ID + " - Alan Gallagher";
		}
		else if(this.ID == "5")
		{
			return ID + " - Maria Gibson";
		}
		
		return returnString;
	}
}
